import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        students: [
            { name: 'ratul', age: 25, grade: 3.5 },
            { name: 'uddin', age: 26, grade: 3.5 },
            { name: 'ashraf', age: 27, grade: 3.5 },
            { name: 'shourov', age: 28, grade: 3.5 },
        ],
        count: 1,
    },
    getters: {
        updateStudent: state => {
            var updateStd = state.students.map(student => {
                return {
                    name: '**' + student.name,
                    age: student.age + 5,
                    grade: student.grade + 1
                }
            });
            return updateStd;
        }
    },
    mutations: {
        increment(state) {
            // mutate state
            state.count++
        },
        handleValue: state => {
            state.students.forEach(student => {
                student.age += 10;
            });
        }
    },

    actions: {
        increment({ commit }) {
            commit('handleValue')
        }
    }
});