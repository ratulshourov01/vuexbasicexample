import Vue from 'vue'
import App from './App.vue'
import Header from './Components/Header_Footer/Header';
import Footer from './Components/Header_Footer/Footer';
import ProductFirst from '@/Components/ProductFirst';
import ProductSecond from '@/Components/ProductSecond';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
Vue.config.productionTip = false
Vue.component('app-header', Header);
Vue.component('app-footer', Footer);
Vue.component('product-first', ProductFirst);
Vue.component('product-second', ProductSecond);
// export const bus = new Vue();
import { store } from './store/store';
new Vue({
    store: store,
    render: h => h(App),
}).$mount('#app')